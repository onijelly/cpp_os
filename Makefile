GPPPARAMS = -m32 -Iinclude -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wno-write-strings
ASPRAMS = --32
LDPARAMS = -melf_i386
.DEFAULT_GOAL := mykernel.iso

objects = objs/loader.o \
		  objs/gdt.o \
		  objs/hardwarecommunication/port.o \
		  objs/hardwarecommunication/interruptstubs.o \
		  objs/hardwarecommunication/interrupts.o \
			objs/hardwarecommunication/pci.o \
		  objs/drivers/keyboard.o \
		  objs/memorymanagement.o \
		  objs/multitasking.o \
		  objs/drivers/mouse.o \
		  objs/drivers/driver.o \
		  objs/drivers/vga.o \
		  objs/drivers/amd_am79c973.o \
		  objs/kernel.o

run: mykernel.iso
	qemu-system-i386 -cdrom $<

objs/%.o : src/%.cpp
	mkdir -p $(@D)
	g++ $(GPPPARAMS) -o $@ -c $<

objs/%.o : src/%.s
	mkdir -p $(@D)
	as $(ASPRAMS) -o $@ $<

mykernel.bin: linker.ld $(objects)
	ld $(LDPARAMS) -T $< -o $@ $(objects)

mykernel.iso: mykernel.bin
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp $< iso/boot/
	echo 'set timeout=0' >> iso/boot/grub/grub.cfg
	echo 'set default=0' >> iso/boot/grub/grub.cfg
	echo '' >> iso/boot/grub/grub.cfg
	echo 'menuentry "My Operating system" {' >> iso/boot/grub/grub.cfg
	echo '	multiboot /boot/mykernel.bin' >> iso/boot/grub/grub.cfg
	echo '	boot' >> iso/boot/grub/grub.cfg
	echo '}' >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=$@ iso
	rm -rf iso
clean:
	rm -f $(objects) mykernel.bin mykernel.iso
