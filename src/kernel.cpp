#include <common/types.h>
#include <drivers/mouse.h>
#include <gdt.h>
#include <hardwarecommunication/interrupts.h>
#include <hardwarecommunication/pci.h>
#include <drivers/keyboard.h>
#include <drivers/driver.h>
#include <drivers/vga.h>
#include <multitasking.h>
#include <memorymanagement.h>

using namespace myos::common;
using myos::GlobalDescriptorTable;
using myos::Task;
using myos::TaskManager;

// drivers namespace
using myos::drivers::DriverManager;
using myos::drivers::KeyboardEventHandler;
using myos::drivers::KeyboardDriver;
using myos::drivers::MouseEventHandler;
using myos::drivers::MouseDriver;
using myos::drivers::VideoGraphicsArray;
using myos::hardwarecommunication::PeripheralComponentInterconnectController;
using myos::MemoryManager;


void printf(char *str) 
{
    // Static storage class
    // Will maintain variable value between function calls
    // instead of being destroyed after each function call.
    static uint16_t *VideoMemory = (uint16_t*)0xb8000;
    static uint8_t x = 0, y = 0;

    for (int i = 0; str[i] != '\0'; ++i) 
    {
        switch(str[i])
        {
            case '\n':
                x = 0;
                y++;
                break;
            default:
                VideoMemory[80 * y + x] = (VideoMemory[80 * y + x] & 0xFF00) | str[i];
                x++;
                break;
        }
    }

    if (x >= 80)
    {
        x = 0;
        y++;
    }

    if (y >= 25) 
    {
        for (y = 0; y < 25; y++)
            for (x = 0; x < 80; x++)
                VideoMemory[80 * y + x] = (VideoMemory[80 * y + x] & 0xFF00) | ' ';
        x = 0;
        y = 0;
    }
}

void printfHex(uint8_t key) 
{
    char *foo = "00";
    char *hex = "0123456789ABCDEF";
    foo[0] = hex[(key >> 4) & 0xF];
    foo[1] = hex[key & 0xF];
    printf(foo);
}

void taskA()
{
    while(true)
        printf("A");
}

void taskB()
{
    while (true)
        printf("B");
}

typedef void (*constructor)(); // define a function pointer
extern "C" constructor start_ctors;
extern "C" constructor end_ctors;

extern "C" void callConstructors() 
{
    for (constructor* i = &start_ctors; i != &end_ctors; i++)
        (*i)();
}

class PrintfKeyBoardEventHandler: public KeyboardEventHandler
{
    public:
        void OnKeyDown(char key_down)
        {
            printf("KEYDOWN ");
            printfHex(key_down);
        }


        void OnKeyUP(char key_up)
        {
            printf("KEYUP ");
            printfHex(key_up);
        }
};

class MouseToConsole: public MouseEventHandler
{
    int8_t x, y;
    public:
    MouseToConsole()
    {
        uint16_t* VideoMemory = (uint16_t*)0xb8000;
        x = 40;
        y = 12;
        VideoMemory[80 * y + x] = (VideoMemory[80 * y + x ] & 0x0F00) << 4
            | (VideoMemory[80 * y + x] & 0xF000) >> 4
            | (VideoMemory[80 * y + x] & 0x00FF);
    }
    void OnMouseDown(uint8_t button)
    {
    }

    void OnMouseUp(uint8_t button) 
    {

    }

    void OnMouseMove(int xoffset, int yoffset)
    {
        uint16_t* VideoMemory = (uint16_t*)0xb8000;
        VideoMemory[80 * y + x] = ((VideoMemory[80 * y + x] & 0xF000) >> 4)
            | ((VideoMemory[80 * y + x] & 0x0F00) << 4)
            | (VideoMemory[80 * y + x] & 0x00FF);

        x += xoffset;
        if (x < 0) x = 0;
        if (x >= 80) x = 79;

        y += yoffset;
        if (y < 0) y = 0;
        if (y >=25) y = 24;

        VideoMemory[80 * y + x] = ((VideoMemory[80 * y + x] & 0xF000) >> 4)
            | ((VideoMemory[80 * y + x] & 0x0F00) << 4)
            | (VideoMemory[80 * y + x] & 0x00FF);

    }

};

extern "C" void kernelMain(void* multiboot_structure, unsigned int magicnumber)
{
    printf("Hello World! --- https://github.com/ita93\n");
    GlobalDescriptorTable gdt;

    uint32_t* memupper = (uint32_t*)(((size_t)multiboot_structure) + 8);
    size_t heap = 10 * 1024 * 1024;
    MemoryManager memoryManager(heap, (*memupper)*1024 - heap - 10 * 1024);

    printf("heap: 0x");
    printfHex((heap >> 24) & 0xFF);
    printfHex((heap >> 16) & 0xFF);
    printfHex((heap >> 8) & 0xFF);
    printfHex((heap) & 0xFF);

    void *allocated = memoryManager.malloc(1024);
    printf("\nallocated: 0x");
    printfHex(((size_t)allocated >> 24) & 0xFF);
    printfHex(((size_t)allocated >> 16) & 0xFF);
    printfHex(((size_t)allocated >> 8) & 0xFF);
    printfHex(((size_t)allocated) & 0xFF);
    printf("\n");

    myos::TaskManager taskManager;
    /*Task task1(&gdt, taskA);
    Task task2(&gdt, taskB);

    taskManager.AddTask(&task1);
    taskManager.AddTask(&task2);
    */

    InterruptManager interrupts(&gdt, &taskManager);

    printf("Hardware initialization stage 1 \n");
    DriverManager drvManager;

    PrintfKeyBoardEventHandler keyboardHandler;
    KeyboardDriver keyboard(&interrupts, 0);
    drvManager.AddDriver(&keyboard);

    MouseToConsole mousehandler;
    MouseDriver mouse(&interrupts, 0);
    drvManager.AddDriver(&mouse);

    PeripheralComponentInterconnectController PCIController;
    PCIController.SelectDrivers(&drvManager, &interrupts);

    //VideoGraphicsArray vga;

    drvManager.ActivateAll();
    printf("Hardware initialization stage 2 \n");

    interrupts.Activate();
    printf("Hardware initialization stage 3 \n");

    /*vga.SetMode(320, 200, 8);
      for (uint8_t y = 0; y < 200; y ++)
      {
      for (uint16_t x = 0;  x < 320; x++)
      {
      vga.PutPixel(x, y, 0x00, 0x00, 0xA8);
      }
      }
      */
    while(1);
}
