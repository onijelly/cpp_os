#include <drivers/keyboard.h>

using myos::drivers::KeyboardEventHandler;
using myos::drivers::KeyboardDriver;

void printf(char*);
void printfHex(uint8_t key);

KeyboardEventHandler::KeyboardEventHandler()
{
}

void KeyboardEventHandler::OnKeyUp(char)
{
}

void KeyboardEventHandler::OnKeyDown(char)
{
}

KeyboardDriver::KeyboardDriver(InterruptManager *manager, KeyboardEventHandler* handler)
    :InterruptHandler(0x21, manager),
    dataport(0x60),
    commandport(0x64)
{
  this->handler = handler;
}

void KeyboardDriver::Activate()
{
  while(commandport.Read() & 0x1)
        dataport.Read();
  commandport.Write(0xAE); //active interrupts
  commandport.Write(0x20);//Give us your current state
  uint8_t status = (commandport.Read() | 1) & ~0x10;
  commandport.Write(0x60); //Set state
  dataport.Write(status);

  dataport.Write(0xF4);
}

KeyboardDriver::~KeyboardDriver()
{
}

uint32_t KeyboardDriver::HandleInterrupt(uint32_t esp)
{
    uint8_t key = dataport.Read();
    if (handler == 0)
      return esp;

    switch (key)
    {
        case 0x45:
        case 0xFA:
        case 0xC5:
            break;
        default:
            handler->OnKeyDown(key); 
            break;
    }
 
    return esp;
}
