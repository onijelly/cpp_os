.set IRQ_BASE, 0x20
.section .text
.extern _ZN4myos21hardwarecommunication16InterruptManager15handleInterruptEhj

.macro HandleException num
.global _ZN4myos21hardwarecommunication16InterruptManager19HandleException\num\()Ev
_ZN4myos21hardwarecommunication16InterruptManager19HandleException\num\()Ev:
    movb $\num, (interruptnumber)
    jmp int_bottom
.endm

.macro HandleInterruptRequest num
.global _ZN4myos21hardwarecommunication16InterruptManager26HandleInterruptRequest\num\()Ev
_ZN4myos21hardwarecommunication16InterruptManager26HandleInterruptRequest\num\()Ev:
    movb $\num + IRQ_BASE, (interruptnumber)
    pushl $0
    jmp int_bottom
.endm

HandleInterruptRequest 0x00
HandleInterruptRequest 0x01
HandleInterruptRequest 0x0C
HandleInterruptRequest 0x09

int_bottom:
    # saving register
    #pusha
    #pushl %ds
    #pushl %es
    #pushl %fs
    #pushl %gs

    pushl %ebp
    pushl %edi
    pushl %esi

    pushl %edx
    pushl %ecx
    pushl %ebx
    pushl %eax

    # store stack pointer before jump to function
    pushl %esp 
    push (interruptnumber)
    call _ZN4myos21hardwarecommunication16InterruptManager15handleInterruptEhj
    # get old esp from return value of handler 
    # add %esp, 6
    mov %eax, %esp

    #restore registers

    #pop %gs
    #pop %fs
    #pop %es
    #pop %ds
    #popa

    popl %eax
    popl %ebx
    popl %ecx
    popl %edx

    popl %esi
    popl %edi
    popl %ebp

    add $4, %esp

    .global _ZN4myos21hardwarecommunication16InterruptManager15InterruptIgnoreEv
    _ZN4myos21hardwarecommunication16InterruptManager15InterruptIgnoreEv:
    iret
.data
    interruptnumber: .byte 0
