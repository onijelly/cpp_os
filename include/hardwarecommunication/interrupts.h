#ifndef __MYOS_HARDWARECOMMUNICATION_INTERRUPTS_H
#define __MYOS_HARDWARECOMMUNICATION_INTERRUPTS_H

#include <common/types.h>
#include <multitasking.h>
#include <hardwarecommunication/port.h>
#include <gdt.h>
#include <hardwarecommunication/interrupts.h>

using namespace myos::common;
using myos::GlobalDescriptorTable;
using myos::TaskManager;

namespace myos
{
    namespace hardwarecommunication
    {
        class InterruptManager;

        class InterruptHandler
        {
            protected:
                uint8_t interruptNumber;
                InterruptManager* interruptManager;
                InterruptHandler(uint8_t interruptNumber, InterruptManager *interruptManager);
                ~InterruptHandler();
            public:
                virtual uint32_t HandleInterrupt(uint32_t esp);
        };

        class InterruptManager
        {
            friend class InterruptHandler;
            protected:
            TaskManager *taskManager;
            static InterruptManager* ActiveInterruptManager;
            InterruptHandler* handlers[256];
            struct GateDescriptor
            {
                uint16_t handlerAddressLowBits;
                uint16_t gdt_codeSegmentSelector;
                uint8_t reserved;
                uint8_t access;
                uint16_t handlerAddressHighBits; 
            }__attribute__((packed));

            static GateDescriptor interruptDescriptorTable[256];

            struct InterruptDescroptorTablePointer
            {
                uint16_t size;
                uint32_t offset;
            }__attribute__((packed));

            static void SetInterruptDescriptorTableEntry(
                    uint8_t interruptNumber,
                    uint16_t gdt_codeSegmentSelectorOffset,
                    void (*handler)(),
                    uint8_t DescriptorPrivilegeLevel,
                    uint8_t DescriptorType
                    );

            Port8BitSlow picMasterCommand;
            Port8BitSlow picMasterData;
            Port8BitSlow picSlaveCommand;
            Port8BitSlow picSlaveData;
            public:
            InterruptManager(GlobalDescriptorTable*, TaskManager*);
            ~InterruptManager();

            void Activate();
            void Deactivate();

            static uint32_t handleInterrupt(uint8_t interruptNumber, uint32_t esp);
            uint32_t DoHandleInterrupt(uint8_t interruptNumber, uint32_t esp);

            static void HandleInterruptRequest();

            static void InterruptIgnore();
            static void HandleInterruptRequest0x00();
            static void HandleInterruptRequest0x01();
            static void HandleInterruptRequest0x0C();
            static void HandleInterruptRequest0x09();
        };
    }
}

#endif //_INTERRUPTS_H
