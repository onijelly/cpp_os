#ifndef __MYOS_MEMORYMANAGEMENT_H
#define __MYOS_MEMORYMANAGEMENT_H

#include <common/types.h>
using namespace myos::common;

namespace myos
{
    struct MemoryChunk
    {
        MemoryChunk *next;
        MemoryChunk *prev;
        bool allocated;
        size_t size; //32bits integer
    };

    class MemoryManager
    {
        protected:
            MemoryChunk* first;
        public:
            static MemoryManager *activeMemoryManager;
            MemoryManager(size_t, size_t);
            ~MemoryManager();
            void *malloc(size_t);
            void free(void* ptr);
    };
}

void* operator new(unsigned size);
void* operator new[](unsigned size);

void* operator new(unsigned size, void *ptr);
void* operator new[](unsigned size, void *ptr);

void operator delete(void* ptr);
void operator delete[](void* ptr);
#endif //__MYOS_MEMORYMANAGEMENT_H
