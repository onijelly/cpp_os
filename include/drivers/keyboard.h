#ifndef __MYOS_DRIVER_KEYBOARD_H
#define __MYOS_DRIVER_KEYBOARD_H

#include <common/types.h>
#include <hardwarecommunication/interrupts.h>
#include <hardwarecommunication/port.h>
#include <drivers/driver.h>

using namespace myos::common;
using myos::hardwarecommunication::Port8Bit;
using myos::hardwarecommunication::InterruptHandler;
using myos::hardwarecommunication::InterruptManager;

namespace myos
{
    namespace drivers
    {

        class KeyboardEventHandler
        {
            public:
                KeyboardEventHandler();
                virtual void OnKeyDown(char);
                virtual void OnKeyUp(char);
        };

        class KeyboardDriver : public InterruptHandler, public Driver
        {
            Port8Bit dataport;
            Port8Bit commandport;
            KeyboardEventHandler* handler;
            public:
            KeyboardDriver(InterruptManager *manager, KeyboardEventHandler *handler);
            ~KeyboardDriver();
            virtual uint32_t HandleInterrupt(uint32_t esp);
            virtual void Activate();
        };

    }
}
#endif //__KEYBOARD_H
