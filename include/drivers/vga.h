#ifndef __MYOS_DRIVERS_VGA_H
#define __MYOS_DRIVERS_VGA_H

#include <common/types.h>
#include <hardwarecommunication/port.h>
#include <drivers/driver.h>

using myos::hardwarecommunication::Port8Bit;
using namespace myos::common;

namespace myos
{
    namespace drivers
    {
        class VideoGraphicsArray
        {
            protected:
                Port8Bit miscPort;
                Port8Bit crtcIndexPort;
                Port8Bit crtcDataPort;
                Port8Bit sequencerIndexPort;
                Port8Bit sequencerDataPort;
                Port8Bit graphicsControllerIndexPort;
                Port8Bit graphicsControllerDataPort;
                Port8Bit attributeControllerIndexPort;
                Port8Bit attributeControllerReadPort;
                Port8Bit attributeControllerWritePort;
                Port8Bit attributeControllerResetPort;
                void WriteRegisters(uint8_t* registers);
                uint8_t* GetFrameBufferSegment();

                virtual uint8_t GetColorIndex(uint8_t, uint8_t, uint8_t);

            public:
                VideoGraphicsArray();
                ~VideoGraphicsArray();
                virtual bool SetMode(uint32_t width, uint32_t height, uint32_t colordepth);
                virtual bool SupportsMode(uint32_t width, uint32_t height, uint32_t colordepth);
                virtual void PutPixel(uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b);
                virtual void PutPixel(uint32_t x, uint32_t y, uint8_t colorIndex);
        };
    }
}

#endif //__MYOS_DRIVERS_VGA_H
